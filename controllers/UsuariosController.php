<?php

namespace app\controllers;

use Yii;
use app\models\Usuarios;
use app\models\UsuariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class UsuariosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'], // Solo usuarios autenticados
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'activate'],
                        'roles' => ['admin'], // Solo administradores
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Usuarios();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idusuario]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $existingUser = Usuarios::findOne(['email' => $model->email]);
            if ($existingUser && $existingUser->idusuario != $id) {
                Yii::$app->session->setFlash('error', 'Este correo electrónico ya está en uso.');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idusuario]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->tipo = 'usuario'; // Cambiar el tipo de usuario como ejemplo de activación

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Usuario activado exitosamente.');
        } else {
            Yii::$app->session->setFlash('error', 'Error al activar el usuario.');
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Usuarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
    }
}
