<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Usuarios;
use yii\web\Controller;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\ErrorAction;

/**
 * SiteController maneja las acciones del sitio.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Muestra la página de inicio de sesión.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', ['model' => $model]);
    }

    /**
     * Muestra la página de registro.
     *
     * @return string
     */
    public function actionRegister()
    {
        $model = new Usuarios();
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->contrasena);
            $model->generateAuthKey();
            $model->tipo = 'usuario';
            if ($model->save()) {
                Yii::$app->user->login($model);
                return $this->goHome();
            }
        }

        return $this->render('register', ['model' => $model]);
    }

    /**
     * Solicita el restablecimiento de la contraseña.
     *
     * @return string
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Revisa tu correo para más instrucciones.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Lo siento, no podemos restablecer la contraseña para el correo electrónico proporcionado.');
            }
        }

        return $this->render('requestPasswordResetToken', ['model' => $model]);
    }

    /**
     * Restablece la contraseña usando el token.
     *
     * @param string $token
     * @return string
     * @throws BadRequestHttpException si el token es inválido
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Nueva contraseña guardada.');
            return $this->goHome();
        }

        return $this->render('resetPassword', ['model' => $model]);
    }
}
