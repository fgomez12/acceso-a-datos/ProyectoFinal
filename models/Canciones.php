<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $idcancion
 * @property string $titulo
 * @property string $album
 * @property string $url_video
 * @property string|null $interprete
 * @property int $idusuario
 *
 * @property Estudios[] $estudios
 * @property Generoscanciones[] $generoscanciones
 * @property Usuarios $idusuario0
 * @property Instrumentoscanciones[] $instrumentoscanciones
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'album', 'url_video', 'idusuario'], 'required'],
            [['idusuario'], 'integer'],
            [['titulo', 'album', 'url_video'], 'string', 'max' => 255],
            [['interprete'], 'string', 'max' => 45],
            [['idusuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['idusuario' => 'idusuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcancion' => 'Idcancion',
            'titulo' => 'Titulo',
            'album' => 'Album',
            'url_video' => 'Url Video',
            'interprete' => 'Interprete',
            'idusuario' => 'Idusuario',
        ];
    }

    /**
     * Gets query for [[Estudios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudios()
    {
        return $this->hasMany(Estudios::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Generoscanciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneroscanciones()
    {
        return $this->hasMany(Generoscanciones::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idusuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuario0()
    {
        return $this->hasOne(Usuarios::class, ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Instrumentoscanciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentoscanciones()
    {
        return $this->hasMany(Instrumentoscanciones::class, ['idcancion' => 'idcancion']);
    }
}
