<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generoscanciones".
 *
 * @property int $idgenero
 * @property int $idcancion
 * @property string $genero
 *
 * @property Canciones $idcancion0
 */
class Generoscanciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'generoscanciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcancion', 'genero'], 'required'],
            [['idcancion'], 'integer'],
            [['genero'], 'string', 'max' => 50],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idcancion' => 'idcancion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idgenero' => 'Idgenero',
            'idcancion' => 'Idcancion',
            'genero' => 'Genero',
        ];
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::class, ['idcancion' => 'idcancion']);
    }
}
