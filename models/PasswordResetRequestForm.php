<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Formulario de solicitud de restablecimiento de contraseña
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @return array las reglas de validación.
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'app\models\Usuarios',
                'filter' => ['status' => 'usuario'],
                'message' => 'No existe ningún usuario con este correo electrónico.'
            ],
        ];
    }

    /**
     * Envía un correo electrónico con un enlace para restablecer la contraseña.
     *
     * @return bool si el correo electrónico fue enviado
     */
    public function sendEmail()
    {
        /* @var $user Usuarios */
        $user = Usuarios::findOne([
            'status' => 'usuario',
            'email' => $this->email,
        ]);

        if ($user) {
            if (!Usuarios::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
                if (!$user->save()) {
                    return false;
                }
            }

            return Yii::$app->mailer->compose(['html' => 'recoverPassword-html', 'text' => 'recoverPassword-text'], ['user' => $user])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                ->setTo($this->email)
                ->setSubject('Restablecimiento de contraseña para ' . Yii::$app->name)
                ->send();
        }

        return false;
    }
}
