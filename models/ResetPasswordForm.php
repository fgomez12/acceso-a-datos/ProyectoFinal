<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\Usuarios;

/**
 * Formulario de restablecimiento de contraseña
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \app\models\Usuarios
     */
    private $_user;

    /**
     * Crea un modelo de formulario dado un token.
     *
     * @param string $token
     * @param array $config pares nombre-valor que se usarán para inicializar las propiedades del objeto
     * @throws InvalidParamException si el token está vacío o no es válido
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('El token de restablecimiento de contraseña no puede estar vacío.');
        }
        $this->_user = Usuarios::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Token de restablecimiento de contraseña incorrecto.');
        }
        parent::__construct($config);
    }

    /**
     * @return array las reglas de validación.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Restablece la contraseña.
     *
     * @return bool si la contraseña fue restablecida.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
