<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm es el modelo detrás del formulario de inicio de sesión.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array las reglas de validación.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Valida la contraseña.
     * Este método sirve como la validación en línea para la contraseña.
     *
     * @param string $attribute el atributo que actualmente se está validando
     * @param array $params los pares nombre-valor adicionales dados en la regla
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Nombre de usuario o contraseña incorrectos.');
            }
        }
    }

    /**
     * Inicia sesión en un usuario utilizando el nombre de usuario y la contraseña proporcionados.
     * @return bool si el usuario inicia sesión correctamente
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    /**
     * Encuentra un usuario por [[username]]
     *
     * @return Usuarios|null
     */
    protected function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Usuarios::findOne(['usuario' => $this->username]);
        }

        return $this->_user;
    }
}
