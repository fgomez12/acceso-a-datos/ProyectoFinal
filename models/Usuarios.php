<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Modelo para la tabla Usuarios.
 */
class Usuarios extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'Usuarios';
    }

    public function rules()
    {
        return [
            [['usuario', 'contrasena', 'email', 'tipo'], 'required'],
            ['email', 'email'],
            [['usuario'], 'string', 'max' => 45],
            [['contrasena'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['tipo'], 'in', 'range' => ['admin', 'usuario']],
            ['usuario', 'unique', 'targetClass' => '\app\models\Usuarios', 'message' => 'Este nombre de usuario ya está en uso.'],
            ['email', 'unique', 'targetClass' => '\app\models\Usuarios', 'message' => 'Este correo electrónico ya está en uso.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'idusuario' => 'ID Usuario',
            'usuario' => 'Usuario',
            'contrasena' => 'Contraseña',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
        ];
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" no está implementado.');
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne(['password_reset_token' => $token]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->contrasena);
    }

    public function setPassword($password)
    {
        $this->contrasena = Yii::$app->security->generatePasswordHash($password);
    }
}
