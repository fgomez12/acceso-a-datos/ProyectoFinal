<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Utilizan $model */

$this->title = 'Update Utilizan: ' . $model->idutilizan;
$this->params['breadcrumbs'][] = ['label' => 'Utilizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idutilizan, 'url' => ['view', 'idutilizan' => $model->idutilizan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="utilizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
