<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Instrumentoscanciones $model */

$this->title = 'Create Instrumentoscanciones';
$this->params['breadcrumbs'][] = ['label' => 'Instrumentoscanciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentoscanciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
