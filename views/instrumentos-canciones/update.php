<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Instrumentoscanciones $model */

$this->title = 'Update Instrumentoscanciones: ' . $model->idinstrumentocancion;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentoscanciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idinstrumentocancion, 'url' => ['view', 'idinstrumentocancion' => $model->idinstrumentocancion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instrumentoscanciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
