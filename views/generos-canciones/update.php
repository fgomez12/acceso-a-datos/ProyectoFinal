<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\GenerosCanciones $model */

$this->title = 'Update Generos Canciones: ' . $model->idgenero;
$this->params['breadcrumbs'][] = ['label' => 'Generos Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idgenero, 'url' => ['view', 'idgenero' => $model->idgenero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="generos-canciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
