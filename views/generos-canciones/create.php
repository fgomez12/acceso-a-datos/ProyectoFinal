<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\GenerosCanciones $model */

$this->title = 'Create Generos Canciones';
$this->params['breadcrumbs'][] = ['label' => 'Generos Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generos-canciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
