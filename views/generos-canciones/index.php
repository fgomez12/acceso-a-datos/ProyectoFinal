<?php

use app\models\GenerosCanciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Generos Canciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generos-canciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Generos Canciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idgenero',
            'idcancion',
            'genero',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, GenerosCanciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idgenero' => $model->idgenero]);
                 }
            ],
        ],
    ]); ?>


</div>
