<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\GenerosCanciones $model */

$this->title = $model->idgenero;
$this->params['breadcrumbs'][] = ['label' => 'Generos Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="generos-canciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idgenero' => $model->idgenero], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idgenero' => $model->idgenero], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idgenero',
            'idcancion',
            'genero',
        ],
    ]) ?>

</div>
