<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registrar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-register">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <img src="C:\xampp\htdocs\DAM\proyectofinal\img\fondo-robot.jpg" alt="BeatBunker" style="width:100%;">
        </div>
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>

                <?= $form->field($model, 'usuario')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'contrasena')->passwordInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tipo')->hiddenInput(['value'=>'usuario'])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Registrar', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
