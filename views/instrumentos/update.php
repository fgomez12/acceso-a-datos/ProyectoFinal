<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */

$this->title = 'Update Instrumentos: ' . $model->idinstrumento;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idinstrumento, 'url' => ['view', 'idinstrumento' => $model->idinstrumento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instrumentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
