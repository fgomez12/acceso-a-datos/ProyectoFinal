<?php

use app\models\Instrumentos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Instrumentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Instrumentos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idinstrumento',
            'tipo',
            'marca',
            'modelo',
            'tamaño',
            //'material',
            //'url:url',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Instrumentos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idinstrumento' => $model->idinstrumento]);
                 }
            ],
        ],
    ]); ?>


</div>
