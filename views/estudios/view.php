<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Estudios $model */

$this->title = $model->idestudio;
$this->params['breadcrumbs'][] = ['label' => 'Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estudios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idestudio' => $model->idestudio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idestudio' => $model->idestudio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idestudio',
            'nombre',
            'web',
            'tecnico_sonido',
            'idcancion',
            'idherramienta',
        ],
    ]) ?>

</div>
