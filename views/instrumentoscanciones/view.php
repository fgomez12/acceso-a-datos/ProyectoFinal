<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Instrumentoscanciones $model */

$this->title = $model->idinstrumentocancion;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentoscanciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="instrumentoscanciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idinstrumentocancion' => $model->idinstrumentocancion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idinstrumentocancion' => $model->idinstrumentocancion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idinstrumentocancion',
            'idinstrumento',
            'idcancion',
        ],
    ]) ?>

</div>
