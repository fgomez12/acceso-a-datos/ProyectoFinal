<?php

use app\models\Instrumentoscanciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Instrumentoscanciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentoscanciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Instrumentoscanciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idinstrumentocancion',
            'idinstrumento',
            'idcancion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Instrumentoscanciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idinstrumentocancion' => $model->idinstrumentocancion]);
                 }
            ],
        ],
    ]); ?>


</div>
