<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */

$this->title = 'Update Canciones: ' . $model->idcancion;
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcancion, 'url' => ['view', 'idcancion' => $model->idcancion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="canciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
