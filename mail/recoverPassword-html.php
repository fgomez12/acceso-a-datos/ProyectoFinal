<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\Usuarios */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hola <?= Html::encode($user->usuario) ?>,</p>

    <p>Haz clic en el siguiente enlace para restablecer tu contraseña:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
