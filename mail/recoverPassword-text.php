<?php

/* @var $this yii\web\View */
/* @var $user app\models\Usuarios */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hola <?= $user->usuario ?>,

Haz clic en el siguiente enlace para restablecer tu contraseña:

<?= $resetLink ?>
